# Deploy infrastructure at Azure Cloud via Terraform
Creates 2 Windows Virual Machines behind Load Balancer.
## Powershell script that deploy new website to IIS WebServer
Example of run Powershell srcipt from local machine to server via WinRM
 - XX.XX.XX.XX - Server IP address
 - YYYY - WinRM Port
 - admin - Username
```powershell
Invoke-Command -ComputerName XX.XX.XX.XX  -Port YYYY -Credential XX.XX.XX.XX\admin -FilePath C:\DeploySitetoIISWebServer.ps1
```