terraform {


  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.18.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "task_rg"
  location = var.location
}

resource "azurerm_virtual_network" "vn" {
  name                = "task_vn"
  address_space       = [var.address_space]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "netsub" {
  name                 = "task_sub"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vn.name
  address_prefixes     = [var.subnet_prefix]
}

resource "azurerm_network_security_group" "nsg" {
  name                = "SecurityGroup-Allow-HTTP-WinRM"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "Allow_HTTP"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow_WinRM"
    priority                   = 170
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow_Traffic_Inside_Subnet"
    priority                   = 150
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = var.subnet_prefix
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "subnetnsg" {
  subnet_id                 = azurerm_subnet.netsub.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_public_ip" "pubip" {
  name                = "publicIPForLB"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_lb" "lb" {
  name                = "loadbalancer"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "publicIP"
    public_ip_address_id = azurerm_public_ip.pubip.id
  }
}

resource "azurerm_network_interface" "netiface" {
  count               = 2
  name                = "networkiface${count.index}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "IP_Configuration"
    subnet_id                     = azurerm_subnet.netsub.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_lb_backend_address_pool" "backaddrpool" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "BackEndAddressPool"
}

resource "azurerm_lb_backend_address_pool_address" "backaddrpooladdr" {
  count                   = 2
  name                    = "BackEndPrivateAddress${count.index}"
  backend_address_pool_id = azurerm_lb_backend_address_pool.backaddrpool.id
  virtual_network_id      = azurerm_virtual_network.vn.id
  ip_address              = azurerm_network_interface.netiface[count.index].private_ip_address

  depends_on = [
    azurerm_lb_backend_address_pool.backaddrpool
  ]
}

resource "azurerm_lb_probe" "lbprobe" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "running-probe"
  port            = 80

  depends_on = [
    azurerm_lb.lb
  ]
}

resource "azurerm_lb_rule" "lbrule" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "LBRule-80"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "publicIP"
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.backaddrpool.id]
  probe_id                       = azurerm_lb_probe.lbprobe.id

  depends_on = [
    azurerm_lb.lb
  ]
}

resource "azurerm_lb_nat_rule" "natrulepool" {
  resource_group_name            = azurerm_resource_group.rg.name
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "RDPAccess-VM"
  protocol                       = "Tcp"
  frontend_port_start            = 61160
  frontend_port_end              = 61165
  backend_port                   = 5985
  backend_address_pool_id        = azurerm_lb_backend_address_pool.backaddrpool.id
  frontend_ip_configuration_name = "publicIP"

  depends_on = [
    azurerm_lb.lb
  ]
}


resource "azurerm_windows_virtual_machine" "windows_vm" {
  count                 = 2
  name                  = "task-vm-${count.index}"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  admin_username        = var.admin_username
  admin_password        = var.admin_password
  network_interface_ids = [element(azurerm_network_interface.netiface.*.id, count.index)]
  size                  = var.windows_vm_size
  zone                  = element(var.availability_zones, count.index)

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
}

resource "azurerm_virtual_machine_extension" "windows_extension" {
  count                = 2
  name                 = "windows_IIS-script${count.index}"
  virtual_machine_id   = azurerm_windows_virtual_machine.windows_vm[count.index].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = <<SETTINGS
    {
        "commandToExecute": "powershell -Command Install-WindowsFeature -name Web-Server -IncludeManagementTools; Set-NetFirewallRule -DisplayName 'Windows Remote Management (HTTP-In)' -RemoteAddress Any; Enable-NetFirewallRule -DisplayName 'Windows Remote Management (HTTP-In)'"
    }
SETTINGS
}