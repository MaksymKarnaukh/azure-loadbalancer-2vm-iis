variable "location" {
  description = "The location/region where resourses is created"
  type        = string
  default     = "West Europe"
}

variable "admin_username" {
  description = "Username of Аdministrator Account"
  type        = string
  default     = "techadmin"
}

variable "admin_password" {
  description = "Administrator password"
  sensitive   = true
  type        = string
}

variable "windows_vm_size" {
  description = "Size/Type of VM"
  type        = string
  default     = "Standard_B2s"
}

variable "availability_zones" {
  description = "List of AZ"
  type        = list(any)
  default     = ["1", "2", "3"]
}

variable "address_space" {
  description = "The address space that is used by the virtual network"
  type        = string
  default     = "10.254.0.0/16"
}

variable "subnet_prefix" {
  description = "The address prefix to use for the subnet"
  type        = string
  default     = "10.254.254.0/24"
}
