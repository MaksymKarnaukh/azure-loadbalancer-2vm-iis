Import-Module WebAdministration 

#Prompt for setting up values
Write-Host "Type the values (Press 'Enter' to use default values)" -ForegroundColor Green
$AppPoolName = Read-Host "Enter name of AppPool [SimpleAppPool]"
$WebSiteName = Read-Host "Enter Web Site Name [SimpleWebSite]"
$WebPort = Read-Host "Enter Port Number of a WebSite[80]"
$UriHTMLFile = Read-Host "Enter URI of HTML File[default]"
$SiteDirPath = "C:\inetpub"

#Set default value for a variables if the user doesn’t enter anything
if ([string]::IsNullOrWhiteSpace($AppPoolName)){
    $AppPoolName = "SimpleAppPool"
}
if ([string]::IsNullOrWhiteSpace($WebSiteName)){
    $WebSiteName = "SimpleWebSite"
}
if ([string]::IsNullOrWhiteSpace($WebPort)){
    $WebPort = "80"
}
if ([string]::IsNullOrWhiteSpace($UriHTMLFile)){
    $UriHTMLFile = "https://gitlab.com/MaksymKarnaukh/azure-loadbalancer-2vm-iis/-/raw/main/simplewebsite/index.html"
}

#Prompt for deleting ALL AppPools
Get-ChildItem IIS:\AppPools\
$titleDelAppPools    = 'Confirm deleting All App Pools'
$questionDelAppPools = 'Do you want to delete All App Pools?'
$choices  = '&Yes', '&No'

$decisionDelAppPooL = $Host.UI.PromptForChoice($titleDelAppPools, $questionDelAppPools, $choices, 1)
if ($decisionDelAppPooL -eq 0) {
    Write-Host 'Deleting All App Pools..' -ForegroundColor Red
    Get-ChildItem IIS:\AppPools | Remove-WebAppPool
}

#Prompt for deleting ALL WebSites
Get-IISSite
$titleDelWebSites    = 'Confirm removing All WebSites'
$questionDelWebSites = 'Do you want to remove All WebSites from IIS?(Files for the website remain intact)'

$decisionWebSites = $Host.UI.PromptForChoice($titleDelWebSites, $questionDelWebSites, $choices, 1)
if ($decisionWebSites -eq 0) {
    Write-Host 'Removing All WebSites..' -ForegroundColor Red
    Get-IISSite | Remove-IISSite -Confirm:$false
}


#Check Application Pool
if(Test-Path IIS:\AppPools\$AppPoolName)
{
    Write-Host "App pool $AppPoolName exists - removing.."
    Remove-WebAppPool $AppPoolName
    #Get-ChildItem IIS:\AppPools
}
Write-Host "Creating new App pool $AppPoolName.." 
New-Item -Path IIS:\AppPools\$AppPoolName | Out-Null

#Check WebSite
if(Test-Path IIS:\Sites\$WebSiteName){
    Write-Host "Website $WebSiteName exists - removing.." -ForegroundColor Red
    Remove-WebSite $WebSiteName
    #Get-ChildItem IIS:\Sites
}

# Delete existing deployment
Write-Host "Checking existing directory and files $SiteDirPath\$WebSiteName..."
if(Test-Path $SiteDirPath\$WebSiteName){
    Write-Host "Directory exists - removing files.." -ForegroundColor Red
    Get-ChildItem $SiteDirPath\$WebSiteName | Remove-Item -Recurse -Force
}
else{
    Write-Host "Creating New Directory $SiteDirPath\$WebSiteName"
    New-Item -ItemType Directory -Name $WebSiteName -Path $SiteDirPath | Out-Null
}

#Download HTML File
Write-Host "Download WebSite File and move to $SiteDirPath\$WebSiteName.."
$SiteFilePath = "$SiteDirPath\$WebSiteName\index.html"
Invoke-WebRequest -Uri $UriHTMLFile -OutFile $SiteFilePath | Out-Null

#Set up Website and add to AppPool
Write-Host "Creating new website.."
New-IISSite -Name $WebSiteName -PhysicalPath $SiteDirPath\$WebSiteName -BindingInformation "*:$($WebPort):" -Force
Set-ItemProperty -Path IIS:\Sites\$WebSiteName -Name applicationPool -Value $AppPoolName 

#Alternative command to set up site(Uncomment this and comment previous 2)
#New-Website -Name $WebSiteName -Port $WebPort -PhysicalPath $SiteDirPath\$WebSiteName -ApplicationPool $AppPoolName

$titleAddIPtoHTML    = "Modify HTML file $SiteFilePath"
$questionAddIPtoHTML = 'Do you want to add IP to index.html file?'

#Modify
$decisionAddIPtoHTML = $Host.UI.PromptForChoice($titleAddIPtoHTML, $questionAddIPtoHTML, $choices, 1)
if ($decisionAddIPtoHTML -eq 0) {
    Write-Host 'Getting IP of server machine..' -ForegroundColor Yellow
    $ServerIPv4 =  (Get-NetIPAddress | Where-Object {$_.AddressState -eq "Preferred" -and $_.AddressFamily -eq "IPv4" -and $_.InterfaceAlias -eq "Ethernet"}).IPAddress
    
    Write-Host "Adding IP to file.." -ForegroundColor Yellow
    ((Get-Content -path $SiteFilePath -Raw) -replace 'script',"script, server IP: $ServerIPv4") | Set-Content -Path $SiteFilePath
    
    #Restart of IIS server(optional)
    #iisreset /restart

}

Get-IISSite $WebSiteName

Write-Host "Complete!" -ForegroundColor Green




